const express = require('express');
const commentsDb = require('../fs/commentsFs');

const router = express.Router();

router.get('/', (req, res) => {

    if (req.query.news_id) {
        const comments = commentsDb.getItemsSpecifiedNews(req.query.news_id);
        return res.send(comments);
    }
    const comments = commentsDb.getItems();
    res.send(comments);
});

router.post('/', (req, res) => {

    const commentReq = req.body;
    if (commentReq.author.length === 0) {
        commentReq.author = 'Anonymous';
    }

    if (commentReq.comment.length === 0 || (commentReq.newsId === undefined || commentReq.newsId.length === 0)) {
        const error = {"error": "comment and newsId must be present in the request"};
        return res.status(400).send(error);
    }
    const result = commentsDb.addItem(commentReq);

    if(result === 'error') {
        const error = {"error": "there is no news with specified ID: " + commentReq.newsId};
        return res.status(400).send(error);
    }
    res.send(result);
});

router.get('/:id', (req, res) => {

    const result = commentsDb.getItemById(req.params.id);
    res.send(result);
});

router.delete('/:id', (req, res) => {

    const result = commentsDb.deleteItemById(req.params.id);
    if(result === 'deleted') {
        res.send(result);
    } else {
        const error = {"error": "there is no location with ID: " + req.params.id};
        return res.status(400).send(error);
    }
});

module.exports = router