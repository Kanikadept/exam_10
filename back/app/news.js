const express = require('express');
const newsDb = require('../fs/newsFs');

const router = express.Router();
const {nanoid} = require('nanoid');
const path = require('path');
const multer = require('multer');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


router.get('/', (req, res) => {

    const categories = newsDb.getItems();
    res.send(categories);
});

router.post('/', upload.single('image'), (req, res) => {

    const news = req.body;

    if(req.file) {
        news.image = req.file.filename;
    }

    if (news.title.length === 0 || news.content.length === 0) {
        const error = {"error": "news title and content must be present in the request"};
        return res.status(400).send(error);
    }
    const result = newsDb.addItem(news);
    res.send(result);
});

router.get('/:id', (req, res) => {

    const result = newsDb.getItemById(req.params.id);
    res.send(result);
});

router.delete('/:id', (req, res) => {

    const result = newsDb.deleteItemById(req.params.id);
    if(result === 'deleted') {
        res.send(result);
    } else {
        const error = {"error": "there is no news with ID: " + req.params.id};
        return res.status(400).send(error);
    }
});

module.exports = router