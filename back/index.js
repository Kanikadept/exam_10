const express = require('express');
//db
const newsDb = require('./fs/newsFs');
const commentsDb = require('./fs/commentsFs');
//rout
const news = require('./app/news');
const comments = require('./app/comments');
//cors
const cors = require("cors");

const app = express();
app.use(express.static('public'))
app.use(express.json());
app.use(cors());


newsDb.init();
commentsDb.init();

const port = 8000;

app.use('/news', news);
app.use('/comments', comments);

app.listen(port, () => {
    console.log(`server port${port}`)
});