const commentsFs = require('fs');
const {nanoid} = require("nanoid");

const fileName = './commentsDb.json';
const newsFileName = './newsDb.json';
let data = [];
let newsData = [];

module.exports = {
    init() {
        try {
            const fileContents = commentsFs.readFileSync(fileName);
            data = JSON.parse(fileContents);

            const newsFileContents = commentsFs.readFileSync(newsFileName);
            newsData = JSON.parse(newsFileContents);
        } catch (e) {
            data = [];
        }
    },

    getItems() {
        return data;
    },

    getItemsSpecifiedNews(id) {
        return data.filter(item => item.newsId === id);
    },

    getItemById(id) {
        return data.find(item => item.id === id);
    },

    deleteItemById(id) {
        const commentToRemove = data.find(item => item.id === id);
        if(commentToRemove) {
            const commentIndex = data.indexOf(commentToRemove);
            data.splice(commentIndex,1);
            this.save();

            return 'deleted';
        } else {
            return 'error';
        }
    },

    addItem(item) {
        const newsExist = newsData.find(news => news.id === item.newsId);
        if (newsExist) {
            item.id = nanoid();
            data.push(item);
            this.save();
            return item;
        } else {
            return 'error';
        }
    },

    save() {
        commentsFs.writeFileSync(fileName, JSON.stringify(data,null, 2));
    }


}