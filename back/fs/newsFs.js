const newsFs = require('fs');
const {nanoid} = require("nanoid");

const fileName = './newsDb.json';
const commentsFileName = './commentsDb.json';
let data = [];
let commentsData = [];

module.exports = {
    init() {
        try {
            const fileContents = newsFs.readFileSync(fileName);
            data = JSON.parse(fileContents);

            const commentsFileContents = newsFs.readFileSync(commentsFileName);
            commentsData = JSON.parse(commentsFileContents);
        } catch (e) {
            data = [];
        }
    },

    getItems() {
        return data;
    },

    getItemById(id) {
        return data.find(item => item.id === id);
    },

    deleteItemById(id) {
        const newsToRemove = data.find(item => item.id === id);
        if(newsToRemove) {
            const newsIndex = data.indexOf(newsToRemove);
            data.splice(newsIndex,1);
            this.save();

            const comments = commentsData.filter(comment => comment.newsId !== id);
            if (comments !== []) {
                newsFs.writeFileSync(commentsFileName, JSON.stringify(comments,null, 2));
            }

            return 'deleted';
        } else {
            return 'error';
        }
    },

    addItem(item) {
        item.id = nanoid();
        item.date = new Date();
        data.push(item);
        this.save();
        return item;
    },

    save() {
        newsFs.writeFileSync(fileName, JSON.stringify(data,null, 2));
    }


}