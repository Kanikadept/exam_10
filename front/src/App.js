import {Switch, Route} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import './App.css';
import Posts from "./containers/Posts/Posts";
import PostForm from "./containers/Posts/PostForm/PostForm";
import SinglePost from "./containers/Posts/SinglePost/SinglePost";

const App = () => (
    <div className="App">
        <Switch>
            <Layout>
                <div className="container">
                    <Route path="/" exact component={Posts}/>
                    <Route path="/addPost" component={PostForm}/>
                    <Route path="/news/:id" component={SinglePost}/>
                </div>
            </Layout>
        </Switch>
    </div>
);

export default App;
