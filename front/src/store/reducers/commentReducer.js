import {CREATE_COMMENT_SUCCESS, DELETE_COMMENT_SUCCESS, FETCH_COMMENTS_SUCCESS} from "../actions/commentsActions";

const initialState = {
    newsComments : [],
}

const commentReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_COMMENT_SUCCESS:
            return {...state, newsComments: [action.comment, ...state.newsComments]};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, newsComments: action.comments};
        case DELETE_COMMENT_SUCCESS:
            const commentsCopy = [...state.newsComments];
            const removeIndex = commentsCopy.map(comment => {
                return comment.id;
            }).indexOf(action.id);
            commentsCopy.splice(removeIndex, 1);
            return {...state, newsComments: commentsCopy}
        default:
            return state;
    }
}

export default commentReducer;