import {
    CREATE_NEW_POST_SUCCESS,
    DELETE_POST_SUCCESS,
    FETCH_POST_SUCCESS,
    FETCH_SINGLE_POST_SUCCESS
} from "../actions/postsActions";


const initialState = {
    posts : [],
    singlePost: null,
}

const postsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POST_SUCCESS:
            return {...state, posts: action.posts};
        case CREATE_NEW_POST_SUCCESS:
            return {...state, posts: [action.post, ...state.posts]};
        case FETCH_SINGLE_POST_SUCCESS:
            return {...state, singlePost: action.post}
        case DELETE_POST_SUCCESS:
            const postsCopy = [...state.posts];
            const removeIndex = postsCopy.map(post => {
                return post.id;
            }).indexOf(action.id);
            postsCopy.splice(removeIndex, 1);
            return {...state, posts: postsCopy}
        default:
            return state;
    }
}

export default postsReducer;