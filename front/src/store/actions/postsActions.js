import axiosApi from "../../axiosApi";

export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const fetchPostSuccess = posts => ({type: FETCH_POST_SUCCESS, posts});
export const fetchPost = () => {
    return async dispatch => {
        try {
            const postsResponse = await axiosApi.get('/news');
            dispatch(fetchPostSuccess(postsResponse.data));
        } catch (e) {
            console.log(e);
        }
    }
}




export const CREATE_NEW_POST_SUCCESS = 'CREATE_NEW_POST_SUCCESS';
export const createNewPostSuccess = (post) => ({type: CREATE_NEW_POST_SUCCESS, post});
export const createNewPost = (post) => {
    return async dispatch => {
        try {
            const postResponse = await axiosApi.post('/news', post);
            dispatch(createNewPostSuccess(postResponse.data));
        } catch (e) {
            console.log(e);
        }
    }
}

export const FETCH_SINGLE_POST_SUCCESS = 'FETCH_SINGLE_POST_SUCCESS';
export const fetchSinglePostSuccess = post => ({type: FETCH_SINGLE_POST_SUCCESS, post});
export const fetchSinglePost = (id) => {
    return async dispatch => {
        try {
            const singlePostResponse = await axiosApi.get('/news/'+ id);
            dispatch(fetchSinglePostSuccess(singlePostResponse.data));
        } catch (e) {
            console.log(e);
        }
    }
}

export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const deletePostSuccess = id => ({type: DELETE_POST_SUCCESS, id});
export const deletePost = (id) => {
    return async dispatch => {
        try {
            const deleteResponse = await axiosApi.delete('/news/'+ id);
            dispatch(deletePostSuccess(deleteResponse));
        } catch (e) {
            console.log(e);
        }
    }
}