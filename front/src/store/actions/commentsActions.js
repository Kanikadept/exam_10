import axiosApi from "../../axiosApi";

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});
export const fetchComments = (id) => {
    return async dispatch => {
        try {
            const commentsResponse = await axiosApi.get('comments', {params: {news_id: id}});
            dispatch(fetchCommentsSuccess(commentsResponse.data));
        } catch (e) {
            console.log(e);
        }
    }
}

export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const createCommentSuccess = comment => ({type: CREATE_COMMENT_SUCCESS, comment});
export const createComment = (comment) => {
    return async dispatch => {
        try {
            const commentResponse = await axiosApi.post('/comments', comment);
            dispatch(createCommentSuccess(commentResponse.data));
        } catch (e) {
            console.log(e);
        }
    }
}

export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const deleteCommentSuccess = id => ({type: DELETE_COMMENT_SUCCESS, id});
export const deleteComment = (id) => {
    return async dispatch => {
        try {
            await axiosApi.delete('/comments/'+ id);
            dispatch(deleteCommentSuccess(id));
        } catch (e) {
            console.log(e);
        }
    }
}