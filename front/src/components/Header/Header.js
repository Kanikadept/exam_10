import React from 'react';
import './Header.css';

const Header = () => {
    return (
        <header className="main-header">
            <div className="container">
                News
            </div>
        </header>
    );
};

export default Header;