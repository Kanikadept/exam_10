import React, {useEffect} from 'react';
import './Posts.css';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../../store/actions/postsActions";
import Post from "./Post/Post";

const Posts = () => {

    const dispatch = useDispatch();
    const posts = useSelector(state => state.postReducer.posts);

    useEffect(() => {
        dispatch(fetchPost());
    }, [dispatch]);

    return (
        <>
            <header className="news-header">
                <div>News</div>
                <NavLink to="/addPost">
                    <button>Add new Post</button>
                </NavLink>
            </header>
            <div className="posts">
                {posts.map(post => {
                    return <Post key={post.id} post={post}/>
                })}
            </div>
        </>

    );
};

export default Posts;