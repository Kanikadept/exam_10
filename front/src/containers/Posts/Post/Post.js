import React from 'react';
import './Post.css';
import {deletePost} from "../../../store/actions/postsActions";
import {useDispatch} from "react-redux";
import {NavLink} from "react-router-dom";

const Post = (props) => {
    const dispatch = useDispatch();


    const handleDeletePost = (id) => {
        dispatch(deletePost(id));
    }

    return (
        <div className="post">
            {props.post.image && (
                <div className="post__image">
                    <img src={'http://localhost:8000/uploads/'+ props.post.image} alt=""/>
                </div>
            )}
            <div className="post__short-info">
                <div className="post__content">{props.post.content}</div>
                <div className="post__actions">
                    <p>{props.post.date}</p>
                    <NavLink to={"news/"+ props.post.id}>
                        <button>Read full Post >></button>
                    </NavLink>
                    <button onClick={() => handleDeletePost(props.post.id)}>Delete</button>
                </div>
            </div>
        </div>
    );
};

export default Post;