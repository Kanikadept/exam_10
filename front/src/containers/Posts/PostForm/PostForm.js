import React, {useState} from 'react';
import './PostForm.css';
import FileInput from "./FileInput/FileInput";
import {useDispatch} from "react-redux";
import {createNewPost} from "../../../store/actions/postsActions";

const PostForm = (props) => {

    const dispatch = useDispatch();

    const [post, setPost] = useState({
        title: '',
        content: '',
        image: ''
    });

    const handleChange = (event) => {
        const {name, value} = event.target;
        const postCopy = {...post};
        postCopy[name] = value;
        setPost(postCopy);
    }

    const handleFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setPost(prevSate => ({
            ...prevSate,
            [name]: file
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(post).forEach(key => {
            formData.append(key, post[key]);
        });
        dispatch(createNewPost(formData));

        setPost({
            title: '',
            content: '',
            image: ''
        });
        props.history.push('/');
    }


    return (
        <>
            <header className="post-from-header">
               <b>Add new Post</b>
            </header>
            <form onSubmit={handleSubmit} className="post-form">
                <div className="form-row">
                    <label>Title</label>
                    <input value={post.title} name="title" onChange={handleChange} type="text"/>
                </div>
                <div className="form-row">
                    <label>Content</label>
                    <textarea value={post.content} onChange={handleChange} name="content" id="content" cols="30" rows="10"/>
                </div>
                <div className="form-row">
                    <label>Image</label>
                    <FileInput name="image" label="Image" onChange={handleFileChange}/>
                </div>
                <button>Save</button>
            </form>
        </>
    );
};

export default PostForm;