import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchSinglePost} from "../../../store/actions/postsActions";
import './SinglePost.css';
import {createComment, deleteComment, fetchComments} from "../../../store/actions/commentsActions";


const SinglePost = (props) => {

    const dispatch = useDispatch();
    const singlePost = useSelector(state => state.postReducer.singlePost);
    const postComments = useSelector(state => state.commentReducer.newsComments);

    const [comment, setComment] = useState({
        author: '',
        comment: '',
    });

    useEffect(() => {
        dispatch(fetchSinglePost(props.match.params.id));
        dispatch(fetchComments(props.match.params.id));
    }, [dispatch,props.match.params.id])

   const handleChange = (event) => {
        const {name, value} = event.target;
        const commentCopy = {...comment};
        commentCopy[name] = value;
        setComment(commentCopy);
   }

   const handleSubmit = (event) => {
       event.preventDefault();
       comment.newsId = props.match.params.id;
       dispatch(createComment(comment));
       setComment({
           author: '',
           comment: '',
       })
   }

   const handleDeleteComment = (id) => {
        dispatch(deleteComment(id));
   }

    return (
        <>
            <div className="single-post">
                <b>{singlePost && singlePost.title}</b>
                <p>{singlePost && singlePost.date}</p>
                <i>{singlePost && singlePost.content}</i>
            </div>
            <b className="comment-part">Comments</b>
            <div className="comments">
                {postComments.map(comment => {
                    return <div key={comment.id} className="comment">
                        <p>{comment.author} wrote: {comment.comment}</p>
                        <button onClick={() => handleDeleteComment(comment.id)}>Delete</button>
                    </div>
                })}
            </div>
            <div className="add-comment">
                <b>Add comment</b>
                <form onSubmit={handleSubmit} className="comment-form">
                    <div className="form-row">
                        <label>Name</label>
                        <input value={comment.author} onChange={handleChange} name="author" type="text"/>
                    </div>
                    <div className="form-row">
                        <label>Comment</label>
                        <input value={comment.comment} onChange={handleChange} name="comment" type="text"/>
                    </div>
                    <button>Add</button>
                </form>
            </div>
        </>

    );
};

export default SinglePost;